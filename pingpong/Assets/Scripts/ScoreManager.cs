using System;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    private int playerScore;

    private int enemyScore;

    public event Action<bool,int> ScoreChanged;

    private void Start()
    {
        playerScore = 0;
        enemyScore = 0;
    }

    public void AddScore(bool isPlayerSide)
    {
        if (isPlayerSide)
        {
            enemyScore += 1;
            ScoreChanged?.Invoke(true,enemyScore);
        }
        else
        {
            playerScore += 1;
            ScoreChanged?.Invoke(false,playerScore);
        }
    }

    public void ResetScore()
    {
        playerScore = 0;
        enemyScore = 0;
    }
}