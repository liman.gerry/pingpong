using UnityEngine;

public class GameManager : MonoBehaviour
{
    private Ball ball;

    private Player player;
    private Enemy enemy;
    
    
    // Start is called before the first frame update
    void Start()
    {
        ball = FindObjectOfType<Ball>();
        player = FindObjectOfType<Player>();
        enemy = FindObjectOfType<Enemy>();
    }

    public void ResetGame()
    {
        ball.ResetBall();
        player.ResetPlayer();
        enemy.ResetEnemy();
    }
}
