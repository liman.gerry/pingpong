using UnityEngine;
using Random = UnityEngine.Random;


public class Ball : KinematicObject
{
    [SerializeField] private float moveSpeed;

    [SerializeField] private Vector3 moveDirection;

    //[SerializeField] private float randomXOffset;

    //[SerializeField] private float randomYOffset;


    protected override void Start()
    {
        RandomizeMoveVariables();
        base.Start();
    }


    protected override void Update()
    {
        Move();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player") || col.CompareTag("Enemy"))
        {
           // float randomXNumber = Random.Range(-1, 2);
            moveDirection.x = (moveDirection.x * -1) * 1.5f; //* Random.value * randomXNumber) ;
            // if (Mathf.Abs(moveDirection.y - transform.position.y) < 0.15f)
            // {
            //     moveDirection.y += 0.15f;
            // }

        }
    }

    private void Move()
    {
        //moveDirection = new Vector3(Random.Range(-2, 1) * moveSpeed, Random.Range(-2, 1) * moveSpeed * Time.deltaTime, 0);

        Vector3 positionAfterMove =
            new Vector3(transform.position.x + moveDirection.x,
                Mathf.Clamp(transform.position.y + moveDirection.y, minYBound, maxYBound), 0f);

        if (positionAfterMove.y >= maxYBound)
        {
            positionAfterMove.y -= moveDirection.y * 2 ;
            moveDirection.y *= -1;
            //positionAfterMove.y = maxYBound;
            //Bounce(positionAfterMove);
            //transform.position = new Vector3(positionAfterMove.x, maxYBound, 0);
        }
        else if (positionAfterMove.y <= minYBound)
        {
            positionAfterMove.y -= moveDirection.y * 2  ;
            moveDirection.y *= -1;
            //positionAfterMove.y = maxYBound;
            //Bounce(positionAfterMove);
            //transform.position = new Vector3(positionAfterMove.x, minYBound, 0);
        }

       
        transform.position = positionAfterMove;
        
    }

    private void RandomizeMoveVariables()
    {
        moveSpeed = Random.Range(-6, 5);
        while (moveSpeed < 3 && moveSpeed > -3)
        {
            moveSpeed = Random.Range(-6, 5);
        }

        float randomXNumber = Random.value; //Random.Range(-1, 2);
        float randomYNumber = Random.value; //Random.Range(-1, 2);

        while (randomXNumber < 0.5f )
        {
            randomXNumber = Random.value; //Random.Range(-1, 2);
        }

        while (randomYNumber < 0.5f )
        {
            randomYNumber = Random.value; //Random.Range(-1, 2);
        }

        moveDirection = new Vector3(randomXNumber * moveSpeed * Time.deltaTime,
            randomYNumber * moveSpeed * Time.deltaTime, 0f);
    }


    public void ResetBall()
    {
        gameObject.SetActive(true);
        transform.position = new Vector3(0, 0, 0);
        RandomizeMoveVariables();
    }
}