using UnityEngine;

public class Player : KinematicObject
{
    [SerializeField] private float moveSpeed =5f;
    [SerializeField] private Vector3 startingPosition;
    
    protected override void Start()
    {
        startingPosition = transform.position;
        base.Start();
    }

    protected override void Update()
    {
        //Vector2.Distance()
        float moveAxis = Input.GetAxis("Vertical");

        base.Move(moveAxis,moveSpeed );
    }

    public void ResetPlayer()
    {
        transform.position = startingPosition;
    }
}
