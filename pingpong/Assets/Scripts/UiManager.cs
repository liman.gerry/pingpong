using TMPro;
using UnityEngine;

public class UiManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI playerScoreText;

    [SerializeField] private TextMeshProUGUI enemyScoreText;

    private ScoreManager scoreManager;

    void Start()
    {
        scoreManager = FindObjectOfType<ScoreManager>();
        scoreManager.ScoreChanged += OnScoreChanged;
        ResetScoreUI();
    }

    private void OnScoreChanged(bool isPlayerSide, int score)
    {
        UpdateScoreUI(isPlayerSide, score);
    }

    private void UpdateScoreUI(bool isPlayerSide, int score)
    {
        if (isPlayerSide)
        {
            enemyScoreText.text = score.ToString();
        }
        else
        {
            playerScoreText.text = score.ToString();
        }
    }

    public void ResetScoreUI()
    {
        enemyScoreText.text = "0";
        playerScoreText.text = "0";
    }
}