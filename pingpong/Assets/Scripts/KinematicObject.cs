using UnityEngine;

public class KinematicObject : MonoBehaviour
{
    protected float minYBound;

    protected float maxYBound;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        Vector3 boundVector = new Vector3(0, 0, 0);
        minYBound = Camera.main.ViewportToWorldPoint(boundVector).y;
        boundVector.y = 1;
        maxYBound = Camera.main.ViewportToWorldPoint(boundVector).y;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
    }

    protected virtual void Move(float moveAxis, float moveSpeed)
    {
        Vector3 moveDirection = new Vector3(0, moveAxis * moveSpeed * Time.deltaTime, 0);

        Vector3 positionAfterMove =
            new Vector3(transform.position.x, Mathf.Clamp(transform.position.y + moveDirection.y, minYBound, maxYBound), 0f);

        
        if (positionAfterMove.y >= maxYBound)
        {
            positionAfterMove.y = maxYBound;
            //transform.position = new Vector3(transform.position.x,maxYBound,0);
        }
        else if(positionAfterMove.y <= minYBound)
        {
            positionAfterMove.y = minYBound;
            //transform.position = new Vector3(transform.position.x,minYBound,0);
        }

        transform.position = positionAfterMove;
    }
}