using System.Collections;
using UnityEngine;

public class Goal : MonoBehaviour
{
    private ScoreManager scoreManager;
   
    [SerializeField] public bool isPlayerSide;

    private void Start()
    {
        scoreManager = FindObjectOfType<ScoreManager>();
       
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.transform.CompareTag("Ball"))
        {
            col.gameObject.SetActive(false);
            scoreManager.AddScore(isPlayerSide);
            StartCoroutine(ResetBall(col.GetComponent<Ball>(),2));
            

        }
       
    }

    private IEnumerator ResetBall(Ball ball, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        ball.ResetBall();
    }


}
