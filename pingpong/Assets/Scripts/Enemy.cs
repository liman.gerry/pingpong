using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : KinematicObject
{
    [SerializeField] public float minMoveDuration;
    [SerializeField] public float maxMoveDuration = 3f;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float moveDuration;
    [SerializeField] private Vector3 startingPosition;
    [SerializeField] private float ballRange = 5f;
    private float randomAxisNumber;
    private float moveAxis;
    private bool isBallInRange;
    private Ball ball;

    protected override void Start()
    {
        isBallInRange = false;
        ball = FindObjectOfType<Ball>();
        startingPosition = transform.position;
        RandomizeMoveDuration();
        base.Start();
    }

    protected override void Update()
    {
        
        isBallInRange = Vector2.Distance(transform.position, ball.transform.position) < ballRange ;
        if (isBallInRange)
        {
            moveAxis = 1;
            if (transform.position.y > ball.transform.position.y)
            {
                moveAxis =-1;
            }
            
            base.Move(moveAxis, moveSpeed);


        }
        else
        {

            moveDuration -= Time.deltaTime;

            if (moveDuration <= 0)
            {
                moveAxis = RandomizeAxisDirection();
                RandomizeMoveDuration();
            }
            else
            {
                base.Move(moveAxis, moveSpeed);
            }
        }

        // else
        // {
        //     Vector2 ballPosition = new Vector2(transform.position.x, ball.transform.position.y); 
        //     transform.position = Vector2.Lerp(transform.position, ballPosition, 0.1f * Time.deltaTime);
        //     //Debug.Log("dsa");
        // }


        //Vector2.Distance()
    }

   

    private float RandomizeAxisDirection()
    {
        randomAxisNumber = Random.Range(-1, 2);
        while (randomAxisNumber == 0)
        {
            randomAxisNumber = Random.Range(-1, 2);
        }

        return randomAxisNumber;
    }

    private void RandomizeMoveDuration()
    {
        moveDuration = Random.Range(minMoveDuration, maxMoveDuration);
    }

    public void ResetEnemy()
    {
        transform.position = startingPosition;
        RandomizeMoveDuration();
    }
    
    
}